FROM node:alpine

RUN apk add --update --no-cache \
      git
RUN apk add --update graphviz --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main

RUN yarn global add madge

WORKDIR /app