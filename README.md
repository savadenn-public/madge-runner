# Madge Runner

This is a dockerized version of the wonderful [Madge](https://github.com/pahen/madge).

`latest` image is rebuilt weekly.

## How to use ?

```
docker run --rm -v "$(pwd):/app" savadenn/madge madge --extensions ts --image dependencies.g.svg --ts-config tsconfig.json ./src
```